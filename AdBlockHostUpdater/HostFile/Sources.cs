﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AdBlockHostUpdater.HostFile
{
  public class Source
  {
    public Source()
    {

    }

    [XmlAttribute]
    public string Name { get; set; }

    [XmlAttribute]
    public string Url { get; set; }
  } // Source

  public class Sources
  {
    public Sources()
    {

    }

    public List<Source> List { get; set; }
  } // Sources
}
