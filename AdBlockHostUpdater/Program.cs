﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AdBlockHostUpdater
{
  class Options
  {
    public Options()
    {
      Interactive = true;
      Level = 0;
    }

    public string OutputFile { get; set; }

    public bool Revert { get; set; }

    public bool Interactive { get; set; }

    public int Level { get; set; }

    public bool IsSystemHostsFile
    {
      get { return OutputFile.Equals(Program.HostFileName, StringComparison.InvariantCultureIgnoreCase); }
    }
  } // Options

  class Program
  {
    public const string HostFileName = @"C:\Windows\System32\drivers\etc\hosts";
    const string HostFileBackupName = "abhu_host.bak";

    static void PrintInfo()
    {
      Console.ForegroundColor = ConsoleColor.White;
      Console.WriteLine("AdBlockHostUpdater");
      Console.ResetColor();
      Console.WriteLine("v.{0}, (c) 2016 Paolo Iommarini, all rights reserved", Assembly.GetExecutingAssembly().GetName().Version);
      Console.WriteLine();
    } // PrintInfo

    static void PrintHelp()
    {
      Console.WriteLine("AdBlockHostUpdater downloads reliable host files and merges them into a single");
      Console.WriteLine("file.");
      Console.WriteLine("By default AdBlockHostUpdater overwrites the system wide hosts file.");
      Console.WriteLine();

      Console.WriteLine("Usage:");
      Console.WriteLine("AdBlockHostUpdater.exe [options]");
      Console.WriteLine();
      Console.WriteLine("--help               Display this help text");
      Console.WriteLine("--level [LEVEL]      Set the security level");
      Console.WriteLine("                     Higher security level includes more hosts");
      Console.WriteLine("                     Available levels: 0, 1, 2, 3");
      Console.WriteLine("                     [Default 0]");
      Console.WriteLine("--noninteractive     Don't ask to press return before exit");
      Console.WriteLine("--revert             Revert changes from the system hosts file");
      Console.WriteLine("--output [filename]  Set the hosts file output name");
      Console.WriteLine("                     [Default {0}]", HostFileName);
    } // PrintHelp

    static int Main(string[] args)
    {
      PrintInfo();

      Options opt = ParseArgs(args);
      if (opt == null) {
        PressReturn(opt);
        return -1;
      }

      opt.OutputFile = Path.GetFullPath(opt.OutputFile);
      string hostFilePath = Path.GetDirectoryName(opt.OutputFile);

      if (opt.IsSystemHostsFile && !IsElevated()) {
        Console.WriteLine("This program requires administrator priviliges, aborting");
        PressReturn(opt);
        return -1;
      }

      // Revert:
      if (opt.Revert) {
        string backupFile = Path.Combine(hostFilePath, HostFileBackupName);
        if (File.Exists(backupFile)) {
          Console.WriteLine("Restoring original hosts file");
          FileInfo fi = new FileInfo(backupFile);
          FileSecurity fs = fi.GetAccessControl();
          fs.SetAccessRuleProtection(true, true);

          if (File.Exists(opt.OutputFile))
            File.Delete(opt.OutputFile);
          File.Move(backupFile, opt.OutputFile);
          fi = new FileInfo(opt.OutputFile);
          fi.SetAccessControl(fs);
        } else {
          Console.WriteLine("Backup file not found, setting default hosts file");
          if (!HostFile.Generator.Generate(null, opt.OutputFile).Result) {
            PressReturn(opt);
            return -1;
          }
        }
        PressReturn(opt);
        return 0;
      }

      if (!Directory.Exists(hostFilePath)) {
        if (opt.IsSystemHostsFile) {
          Console.WriteLine("Hosts file not found, aborting");
          PressReturn(opt);
          return -1;
        } else {
          try {
            Directory.CreateDirectory(hostFilePath);
          } catch (Exception ex) {
            Console.WriteLine("Failed to create folder {0}", hostFilePath);
            Console.WriteLine("{0}", ex.Message);
            PressReturn(opt);
            return -1;
          }
        }
      }

      HostFile.Sources s = new HostFile.Sources()
      {
        List = new List<HostFile.Source>()
        {
          new HostFile.Source() { Name = "adaway.org", Url="http://adaway.org/hosts.txt"  },
          new HostFile.Source() { Name = "mvps.org", Url="http://winhelp2002.mvps.org/hosts.txt"  },
          new HostFile.Source() { Name = "someonewhocares.org", Url="http://someonewhocares.org/hosts/hosts"  },
          new HostFile.Source() { Name = "malwaredomainlist.com", Url="http://www.malwaredomainlist.com/hostslist/hosts.txt"  },
          new HostFile.Source() { Name = "pgl.yoyo.org", Url="http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext"  },          
        }
      };

      if (opt.Level >= 1) {
        s.List.Add(new HostFile.Source() { Name = "hpHosts::ATS", Url = "http://hosts-file.net/ad_servers.txt" });
      }

      if (opt.Level >= 2) {
        s.List.Add(new HostFile.Source() { Name = "hpHosts::EMD", Url = "http://hosts-file.net/emd.txt" });
        s.List.Add(new HostFile.Source() { Name = "hpHosts::EXP", Url = "http://hosts-file.net/exp.txt" });
      }

      if (opt.Level >= 3) {
        s.List.Add(new HostFile.Source() { Name = "hpHosts::FSA", Url = "http://hosts-file.net/fsa.txt" });
        s.List.Add(new HostFile.Source() { Name = "hpHosts::GRM", Url = "http://hosts-file.net/grm.txt" });
      }

      string tempFile = Path.GetTempFileName();
      if (HostFile.Generator.Generate(s, tempFile).Result) {        
        Console.WriteLine("Copying hosts file");
        try {
          FileSecurity fs = null;

          // Backup original file:
          if (opt.IsSystemHostsFile) {
            if (File.Exists(opt.OutputFile)) {
              FileInfo fi = new FileInfo(opt.OutputFile);
              fs = fi.GetAccessControl();
              fs.SetAccessRuleProtection(true, true);
            }
            string backupFile = Path.Combine(hostFilePath, HostFileBackupName);
            if (!File.Exists(backupFile)) {
              File.Copy(opt.OutputFile, backupFile);
              FileInfo fi = new FileInfo(backupFile);
              fi.SetAccessControl(fs);
            }
          }

          if (File.Exists(opt.OutputFile))
            File.Delete(opt.OutputFile);
          File.Move(tempFile, opt.OutputFile);
          if (fs != null) {
            // Restore file permissions
            FileInfo fi = new FileInfo(opt.OutputFile);
            fi.SetAccessControl(fs);
          }
        } catch (Exception ex) {
          Console.WriteLine("Failed to copy hosts file");
          Console.WriteLine("{0}", ex.Message);
          PressReturn(opt);
          return -1;
        }
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Hosts file updated");
        Console.ResetColor();
      } else {
        PressReturn(opt);
        return -1;
      }

      PressReturn(opt);
      return 0;
    } // Main

    private static void PressReturn(Options opt)
    {
      if (opt == null || opt.Interactive) {
        Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine("Press RETURN to exit");
        Console.ResetColor();
        Console.ReadLine();
      }
    } // PressReturn

    private static bool IsElevated()
    {
      WindowsIdentity identity = WindowsIdentity.GetCurrent();
      WindowsPrincipal principal = new WindowsPrincipal(identity);
      return principal.IsInRole(WindowsBuiltInRole.Administrator);
    } // IsElevated

    private static Options ParseArgs(string[] args)
    {
      Options res = new Options() { OutputFile = HostFileName };

      for (int i = 0; i < args.Length; i++) {
        string arg = args[i];

        if (arg.Equals("--output", StringComparison.InvariantCultureIgnoreCase)) {
          if (i < arg.Length - 1) {
            res.OutputFile = args[++i];
          } else {
            Console.WriteLine("Missing argument for --output");
            return null;
          }
        } else if (arg.Equals("--level", StringComparison.InvariantCultureIgnoreCase)) {
          if (i < arg.Length - 1) {
            int lvl = 0;
            if (int.TryParse(args[++i], out lvl))
              res.Level = lvl;
            else {
              Console.WriteLine("Invalid value for --level");
              return null;
            }
          } else {
            Console.WriteLine("Missing argument for --level");
            return null;
          }
        } else if (arg.Equals("--revert", StringComparison.InvariantCultureIgnoreCase)) {
          res.Revert = true;
        } else if (arg.Equals("--noninteractive", StringComparison.InvariantCultureIgnoreCase)) {
          res.Interactive = false;
        } else if (arg.Equals("--help", StringComparison.InvariantCultureIgnoreCase)) {
          PrintHelp();
          return null;
        } else {
          Console.WriteLine("Unknown option {0}", arg);
          return null;
        }
      }

      // Ensure that "revert" points to the system file:
      if (res.Revert)
        res.OutputFile = HostFileName;
      return res;
    } // ParseArgs
  }
}
