# AdBlockHostUpdater #

Protect your system from malware, spyware and block ads by using a high quality hosts file.

This command line utility downloads some high quality host files from the web and merges them in a single hosts file.

To update your system hosts file download the utility, unzip it where you want and execute it with administrator privileges.